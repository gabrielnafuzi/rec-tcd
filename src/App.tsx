import React, { useState } from 'react'

import { StatusBar } from 'expo-status-bar'
import { Provider as PaperProvider, BottomNavigation } from 'react-native-paper'
import Toast from 'react-native-toast-message'

import { ListScreen, RegisterScreen } from '@/screens'

const routes = [
  { key: 'list', title: 'Listagem', icon: 'car', component: ListScreen },
  {
    key: 'register',
    title: 'Cadastrar',
    icon: 'plus',
    component: RegisterScreen
  }
]

export default function App() {
  const [index, setIndex] = useState(0)

  return (
    <PaperProvider>
      <BottomNavigation
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        renderScene={({ route }) => {
          if (routes[index].key !== route.key) return null

          const Component = routes[index].component

          return <Component />
        }}
        shifting
      />

      <StatusBar style="light" />

      <Toast />
    </PaperProvider>
  )
}
