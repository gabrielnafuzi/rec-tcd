export type VehicleCategory =
  | 'sedan'
  | 'suv'
  | 'camionete'
  | 'hatch'
  | 'motocicleta'

export type Vehicle = {
  id?: number
  category: VehicleCategory
  minimumPrice: string
  entryDate: string
  description: string
}
