import * as SQLite from 'expo-sqlite'

const DB_NAME = '@rec_pdm.db'
export const db = SQLite.openDatabase(DB_NAME)
