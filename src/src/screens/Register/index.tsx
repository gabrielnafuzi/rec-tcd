import React, { useCallback, useEffect, useState } from 'react'

import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native'

import { yupResolver } from '@hookform/resolvers/yup'
import dayjs from 'dayjs'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { TextInputMask } from 'react-native-masked-text'
import { Appbar, Button, HelperText, TextInput } from 'react-native-paper'
import DropDown from 'react-native-paper-dropdown'
import Toast from 'react-native-toast-message'
import * as yup from 'yup'

import { categories } from '@/constants'
import { db } from '@/services/database'
import { Vehicle } from '@/services/types'

const dateRegex = /^(0?[1-9]|[12][0-9]|3[01])[/-](0?[1-9]|1[012])[/-]\d{4}$/

const formSchema = yup.object({
  category: yup.string().required('Categoria é obrigatório'),
  minimumPrice: yup.string().required('Preço mínimo é obrigatório'),
  entryDate: yup
    .string()
    .matches(dateRegex, 'Data de entrada inválida')
    .required('Data de entrada é obrigatório'),
  description: yup.string().required('Descrição é obrigatório')
})

const useForceUpdate = () => {
  const [value, setValue] = useState(0)

  return [() => setValue(value + 1), value] as const
}

export const RegisterScreen = () => {
  const [showDropDown, setShowDropDown] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [forceUpdate, forceUpdateId] = useForceUpdate()

  const { control, setValue, handleSubmit, reset } = useForm<Vehicle>({
    resolver: yupResolver(formSchema),
    mode: 'onChange'
  })

  const handleSetTodayOnEntryDate = useCallback(() => {
    setValue('entryDate', dayjs().format('DD/MM/YYYY'), {
      shouldValidate: true
    })
  }, [setValue])

  const handleAddNewVehicle: SubmitHandler<Vehicle> = (data) => {
    setIsSubmitting(true)

    db.transaction(
      (tx) => {
        tx.executeSql(
          `INSERT INTO vehicles (category, minimumPrice, entryDate, description) VALUES (?, ?, ?, ?)`,
          [data.category, data.minimumPrice, data.entryDate, data.description]
        )

        setIsSubmitting(false)
      },
      () => {
        Toast.show({
          text1: 'Algo deu errado, tente novamente! 😢',
          text2: 'Caso o erro persista, feche o aplicativo e abra novamente.',
          type: 'error'
        })
      },
      () => {
        Toast.show({
          text1: 'Veículo cadastrado com sucesso! 🚘✅',
          text2: 'Vá para a listagem para ver o novo veículo! 🚀',
          type: 'success'
        })

        reset({
          category: undefined,
          minimumPrice: '',
          entryDate: '',
          description: ''
        })

        forceUpdate()
      }
    )
  }

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        'create table if not exists vehicles (id integer primary key not null, category text, minimumPrice text, entryDate text, description text);'
      )
    })
  }, [])

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.Content
          title="Cadastrar veículo"
          style={{ alignItems: 'center' }}
        />
      </Appbar.Header>

      <ScrollView>
        <SafeAreaView style={styles.safeContainerStyle}>
          <Controller
            control={control}
            name="category"
            render={({ field: { value, onChange }, formState: { errors } }) => (
              <>
                <DropDown
                  label="Categorias"
                  list={categories}
                  value={value}
                  mode="outlined"
                  visible={showDropDown}
                  showDropDown={() => setShowDropDown(true)}
                  onDismiss={() => setShowDropDown(false)}
                  setValue={onChange}
                  inputProps={{ error: !!errors.category }}
                  key={`forceupdate-dropwdown-${forceUpdateId}`}
                />

                <HelperText type="error" visible={!!errors.category}>
                  {errors.category?.message}
                </HelperText>
              </>
            )}
          />

          <Controller
            control={control}
            name="minimumPrice"
            render={({ field: { value, onChange }, formState: { errors } }) => (
              <>
                <TextInput
                  mode="outlined"
                  label="Valor mínimo"
                  value={value}
                  onChangeText={onChange}
                  error={!!errors.minimumPrice}
                  render={({ ref: _, ...props }) => (
                    <TextInputMask
                      type="money"
                      options={{
                        unit: 'R$ '
                      }}
                      {...props}
                    />
                  )}
                />

                <HelperText type="error" visible={!!errors.minimumPrice}>
                  {errors.minimumPrice?.message}
                </HelperText>
              </>
            )}
          />

          <Controller
            control={control}
            name="entryDate"
            render={({ field: { value, onChange }, formState: { errors } }) => (
              <>
                <View style={styles.entryDateContainer}>
                  <TextInput
                    mode="outlined"
                    label="Data de entrada"
                    value={value}
                    onChangeText={onChange}
                    error={!!errors.entryDate}
                    style={{ width: '70%' }}
                    render={({ ref: _, ...props }) => (
                      <TextInputMask
                        type="datetime"
                        options={{ format: 'DD/MM/YYYY' }}
                        {...props}
                      />
                    )}
                  />

                  <Button onPress={handleSetTodayOnEntryDate} mode="contained">
                    Hoje
                  </Button>
                </View>

                <HelperText type="error" visible={!!errors.entryDate}>
                  {errors.entryDate?.message}
                </HelperText>
              </>
            )}
          />

          <Controller
            control={control}
            name="description"
            render={({ field: { value, onChange }, formState: { errors } }) => (
              <>
                <TextInput
                  mode="outlined"
                  label="Descrição"
                  value={value}
                  onChangeText={onChange}
                  error={!!errors.description}
                />

                <HelperText type="error" visible={!!errors.description}>
                  {errors.description?.message}
                </HelperText>
              </>
            )}
          />

          <Button
            mode="contained"
            style={styles.addNewVehicleButton}
            onPress={handleSubmit(handleAddNewVehicle)}
            loading={isSubmitting}
            disabled={isSubmitting}
          >
            Adicionar
          </Button>
        </SafeAreaView>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  safeContainerStyle: {
    flex: 1,
    margin: 20,
    justifyContent: 'center'
  },
  addNewVehicleButton: {
    marginTop: 20
  },
  entryDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
})
