import React, { useCallback, useEffect, useState } from 'react'

import { SafeAreaView, ScrollView, View } from 'react-native'

import { Appbar, List, Headline } from 'react-native-paper'

import { categories } from '@/constants'
import { db } from '@/services/database'
import { Vehicle, VehicleCategory } from '@/services/types'

export const ListScreen = () => {
  const [vehicles, setVehicles] = useState<Vehicle[]>([])

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql('select * from vehicles', [], (_, { rows }) => {
        setVehicles(rows._array)
      })
    })
  }, [])

  const getCategory = useCallback(
    (category: VehicleCategory) =>
      categories.find((c) => c.value === category)?.label,
    []
  )

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Appbar.Header>
        <Appbar.Content
          title="Listagem de veículos"
          style={{ alignItems: 'center' }}
        />
      </Appbar.Header>

      {vehicles.length ? (
        <ScrollView>
          <List.Section>
            {vehicles?.map((vehicle) => (
              <List.Accordion
                title={`${getCategory(vehicle.category)} - ${
                  vehicle.minimumPrice
                }`}
                description={`Data de entrada: ${vehicle.entryDate}`}
                key={vehicle.id}
              >
                <List.Item
                  title="Descrição: "
                  description={vehicle.description}
                  descriptionNumberOfLines={10}
                />
              </List.Accordion>
            ))}
          </List.Section>
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Headline>Nenhum veículo cadastrado</Headline>
        </View>
      )}
    </SafeAreaView>
  )
}
