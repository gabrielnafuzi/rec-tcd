import { VehicleCategory } from '@/services/types'

type VehicleCategoryLabel =
  | 'Sedan'
  | 'SUV'
  | 'Camionete'
  | 'Hatch'
  | 'Motocicleta'

type Category = {
  value: VehicleCategory
  label: VehicleCategoryLabel
}

export const categories: Category[] = [
  { value: 'sedan', label: 'Sedan' },
  { value: 'suv', label: 'SUV' },
  { value: 'camionete', label: 'Camionete' },
  { value: 'hatch', label: 'Hatch' },
  { value: 'motocicleta', label: 'Motocicleta' }
]
